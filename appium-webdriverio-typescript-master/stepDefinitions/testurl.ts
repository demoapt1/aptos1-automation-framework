//import { SearchPageObject } from "../pages/searchPage";
import { RestUtil} from "../utils/restUtil";


import * as restm from 'typed-rest-client/RestClient';
import * as util from 'typed-rest-client/Util';
import * as fs from 'fs';
import * as path from 'path';

import 'url-search-params-polyfill';
const got = require('got');
const assert = require('assert');

//var chakram = require('chakram');
var request = require('then-request');
const tunnel = require('tunnel');

const { Before,After ,Status, Given ,Then} = require("cucumber");
//const chai = require("chai").use(require("chai-as-promised"));
//const expect = chai.expect;

//const search: SearchPageObject = new SearchPageObject();
const restobj: RestUtil = new RestUtil();

export interface HttpBinData {
    url: string;
    data: any;
    json: any;
    args?: any
}


Given(/^open my url$/, async () => {
  await  browser.url("https://www.google.com?q=test");
    console.log("HOME PAGE &&&&88888");
    
});

/**
 * Hook for the new
 */
/*After((scenarioResult)=>{
    // Here it is added to a failed step, but each time you call `browser.saveScreenshot()` it will automatically bee added to the report
  //  if (scenarioResult.result.status === Status.FAILED) {
        // It will add the screenshot to the JSON
        browser.saveScreenshot('./tmp/screenshot.png');
        
    //}
    return scenarioResult.status;
});*/
/*Given(/^invoke rest call for test$/, async()=>{
console.log("invoke rest");
var htmlstring = null;
/*await restobj.loadHtmlUrl('http://google.com', function(rawEncode) {
  htmlstring = rawEncode;
  //Do your stuff here...
  console.log("Inside sytep  "+rawEncode);
  
});*/
   // await restobj.disp();
 /* await restobj.resttest('https://api.blockcypher.com/',function(responsedata){
    console.log("Inside sytep  "+responsedata);
  });
});*/

Given(/^invoke rest call for test$/, async() => {
   await restobj.disp();
   // this.timeout(3000);
});
   /*let _rest: restm.RestClient;
   let _restBin: restm.RestClient;
   _rest = new restm.RestClient('typed-rest-client-tests');
    _restBin = new restm.RestClient('typed-rest-client-tests', 'https://httpbin.org');

   let restRes: restm.IRestResponse<HttpBinData> = await _rest.get<HttpBinData>('https://httpbin.org/get');
   assert(restRes.statusCode == 200, "statusCode should be 200");
   assert(restRes.result && restRes.result.url === 'https://httpbin.org/get');*/
   
 //  var result =request('GET', 'http://example.com');
//console.log('RESULT'+result.get.responsedata);
   


Then(/^send GET request to "([^"]*)", user name should be "([^"]*)"$/, async function (url, name) {

    var paramsString = "q=isbn:9781451648546";
  //  const query = new URLSearchParams(paramsString);

//  const  search1 = new URLSearchParams();

  const query = new URLSearchParams();
      query.append('q', 'isbn:9781451648546');
  /*let res=  await got(url,{query, headers: {
    
    'Accept': 'application/json'
},agent: tunnel.httpOverHttp({
    proxy: {
        host: 'localhost'
    }
})

});*/

let res=  await got(url,{query, headers: {
    
    'Accept': 'application/json'
},

method: 'GET'
});

 // search1.set('q', 'isbn:0521609283');
//const  theURL= new URL(url);
//let params = new URLSearchParams(theURL.search);

  //let res=  await got(url+'?q=isbn:9781451648546');

 // console.log(search1.toString());
//    let res = await got.get(url);
    
   console.log("RESPONSE BODY "+res.body);
    let json = JSON.parse(res.body);
  /*  let nameAgeMapping = new Map();
 
for (let key of nameAgeMapping.keys()) {
    console.log(key);                   //Lokesh Raj John
}*/


   console.log("TILE "+ json.items[0].volumeInfo.title);
    return assert.equal(json.items[0].volumeInfo.title , 'Steve Jobs');


    
});


Then(/^send POST request to "([^"]*)", input body is$/, async function (url, inputbody:String) {

    let res=  await got(url,{ headers: {
    
        'Accept': 'application/json'
    },
    
    method: 'POST',
    body: inputbody,
}


);
console.log(res.body);
let json = JSON.parse(res.body);
console.log("POST DATA "+ json);
});
    