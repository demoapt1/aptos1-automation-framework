/**
 * Step Definitons are the glue code which drive
 * the feature scenarios, Cucumber helps us provide
 * gherkin language syntax's - Given, When, Then
 */

const {Given, When, Then} = require('cucumber');
import {expect} from 'chai';
import {CalculatorPageObject} from '../pages/calcPage';
import { threadId } from 'worker_threads';

const calc: CalculatorPageObject = new CalculatorPageObject();

Given(/^I am on my mobile calculator app$/, async () =>{
    
        await new Promise(resolve => setTimeout(()=>resolve(), 10000)).then(()=>console.log("fired"));
        
        //const selector = 'new UiSelector().text("Exit").className("com.penit.rob")';
//const Button = $(`android=${selector}`);
//Button.click();
browser.click('android=new UiSelector().text("Exit").className("android.widget.Button")');
//$('android=new UiSelector().text("Exit").className("android.widget.Button")').click();
  //  browser.click(Button);
  /*  const title = browser.getText('android.widget.TextView');
    expect(title).to.equal('Calculator');*/
});

When(/^I add "(.*?)" and "(.*?)"$/,  async () => {

    await new Promise(resolve => setTimeout(()=>resolve(), 3000)).then(()=>console.log("fired new"));
  /*  browser.click(calc.calcDigitSelector(num1));
    browser.click(calc.addOperator);
    browser.click(calc.calcDigitSelector(num2));
    browser.click(calc.equalOperator);*/
});

When(/^I subtract "(.*?)" from "(.*?)"$/,  (num1: string, num2: string) => {
  /*  browser.click(calc.calcDigitSelector(num1));
    browser.click(calc.subtractOperator);
    browser.click(calc.calcDigitSelector(num2));
    browser.click(calc.equalOperator);*/
});

When(/^I multiply "(.*?)" with "(.*?)"$/,  (num1: string, num2: string) => {
 /*   browser.click(calc.calcDigitSelector(num1));
    browser.click(calc.multiplyOperator);
    browser.click(calc.calcDigitSelector(num2));
    browser.click(calc.equalOperator);*/
});

When(/^I divide "(.*?)" with "(.*?)"$/,  (num1: string, num2: string) => {
 /*   browser.click(calc.calcDigitSelector(num1));
    browser.click(calc.divisionOperator);
    browser.click(calc.calcDigitSelector(num2));
    browser.click(calc.equalOperator);*/
});

When(/^I click on AC button$/, () => {
  /*  browser.click(calc.clearOperator);*?
});

Then(/^the result "(.*?)" should be displayed$/, (result: string) => {
   /* return expect(browser.getText(calc.outputText)).to.contain(result);*/
});

Then(/^the result should be cleared$/, () => {
  /*  return expect(browser.getText(calc.outputText)).to.equal('');*/
});
