/**
 * WebdriverIO config file to run tests on native mobile apps.
 * Config file helps us configure all the settings and setup environments 
 * to run our tests.
 */
//const report = require('multiple-cucumber-html-reporter');

const host = '127.0.0.1';   // default appium host
const port = 4723;          // default appium port

const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;

exports.config = {
    debug: false,
    specs: [
        './features/iosapp.feature',
    ],

    reporters: ['allure','spec'],
    reporterOptions: {
        allure: {
           outputDir: './allure-results/'
        }
    },

  /*  reporters: ['multiple-cucumber-html'],
    reporterOptions: {
        htmlReporter: {
            jsonFolder: './tmp',
            reportFolder: `./tmp/report`,
            // ... other options, see Options
        }
    },
*/
    host: host,
    port: port,

    maxInstances: 1,

    capabilities: [
        {
            //appiumVersion: '1.10.0',                 // Appium module version
            //browserName: '',                        // browser name is empty for native apps
            //platformName: 'Android',
            //app: './app/apkfiles.com_427875_Pen-It.apk',          // Path to your native app
            //appPackage: 'com.penit.rob',  // Package name of your app
            //appActivity: 'com.penit.rob.InfoActivity', // App activity of the app
            //platformVersion: '7.1.1',              // Android platform version of the device
            //deviceName: 'emulator-5554',              // device name of the mobile device
           // waitforTimeout: waitforTimeout,
         //   commandTimeout: commandTimeout,
            newCommandTimeout: 30 * 60000,
            deviceName: 'iPhone 6',
            platformName: 'iOS',
            platformVersion: '12.1',
            orientation: 'PORTRAIT',
            // The path to the app
            
            app:  '/Users/kd51403/Desktop/webdriverioappium/appium-boilerplate-master/app/wdioDemoApp.app',
            // Read the reset strategies very well, they differ per platform, see
            // http://appium.io/docs/en/writing-running-appium/other/reset-strategies/
            noReset: true,
            newCommandTimeout: 240,
        }
    ],

    services: ['appium'],
    appium: {
        waitStartTime: 6000,
        waitforTimeout: waitforTimeout,
        command: 'appium',
        logFileName: 'appium.log',
        args: {
            address: host,
            port: port,
            commandTimeout: commandTimeout,
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    /**
     * test configurations
     */
    logLevel: 'silent',
    coloredLogs: true,
    framework: 'cucumber',          // cucumber framework specified 
    cucumberOpts: {
        compiler: ['ts:ts-node/register'],
        backtrace: true,
        failFast: false,
        timeout: 5 * 60 * 60000,
        require: ['./stepDefinitions/iosapp.ts']      // importing/requiring step definition files
    },

    /**
     * hooks help us execute the repeatitive and common utilities 
     * of the project.
     */
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');
//        reporter_1.Reporter.createDirectory(jsonReports);
    },
    
    afterScenario: function (scenario) {
        browser.screenshot();
     },

    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
        onComplete: () => {
          //  reporter_1.Reporter.createHTMLReport();
          report.generate({
            jsonDir: './tmp/json-output/',
            reportPath: './tmp/report/'
          });
        }
    }

};
