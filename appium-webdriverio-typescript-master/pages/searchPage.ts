//import { $, ElementFinder } from "protractor";

export class SearchPageObject {
    //public searchTextBox: ElementFinder;
    //public searchButton: ElementFinder;
    //public logo: ElementFinder;
    public get searchTextBox()  { return browser.element("//input[title='Search']") }
    public get logo()  { return browser.element("#logo > img") }
    public get searchButton()  { return browser.element("//input[value='Google Search']") }
   // public get logoutButton()  { return browser.element("//i[@class='icon-2x icon-signout']") }
    //public get successMessage()  { return browser.element("//div[@class='flash success']") }
    //public get errorMessage()  { return browser.element("//div[@class='flash error']") }
    constructor() {
      //  this.searchTextBox = $("input[title='Search']");
       // this.searchButton = $("input[value='Google Search']");
        //this.logo = $("#logo > img");
    }

   public open(): void {
        browser.url("https://www.google.com");
    }
}
const SearchPageObj = new SearchPageObject();
export default SearchPageObj;