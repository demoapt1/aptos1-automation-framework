Feature: Get book by ISBN


  Scenario: User calls web service to get a book by its ISBN
	Given send GET request to "https://www.googleapis.com/books/v1/volumes", with an isbn of 9781451648546
	Then the status code is 200
     Then response includes the following
    | totalItems                                                           | 1                       |
    | kind                                                                 | books#volumes           |
    | items[0].volumeInfo.publisher                                        | Simon and Schuster      |   
    | items[0].volumeInfo.industryIdentifiers[1].identifier                | 1451648545              |
    |items[0].volumeInfo.categories                                        |Biography & Autobiography|

Scenario: Make sample post call

Then send POST request to "http://httpbin.org/post", input body is
"""
{
    "foo":"bar"
 }
""""